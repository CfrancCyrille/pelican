#!/usr/bin/env python
from __future__ import print_function
import os

try:
    from pelicanconf import OUTPUT_PATH

    if os.path.isdir(OUTPUT_PATH):
        os.system('pelican content --output %s --settings publishconf.py' % OUTPUT_PATH)
    else:
        print('Error: %s is invalid path' % OUTPUT_PATH)
except ImportError:
    print('Error: failed to import pelicanconf.py settings')
