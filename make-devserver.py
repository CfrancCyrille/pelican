#!/usr/bin/env python
from __future__ import print_function
import os

curdir = os.getcwd()

try:
    from pelicanconf import OUTPUT_PATH

    if os.path.isdir(OUTPUT_PATH):
        os.chdir(OUTPUT_PATH)
        print('Starting Pelican devserver. Press CTRL+C to stop.\n')
        os.system('python -m pelican.server')
        os.chdir(curdir)
    else:
        print('Error: %s is invalid path' % OUTPUT_PATH)
except ImportError:
    print('Error: failed to import pelicanconf.py settings')

except WindowsError:
    """
    On Windows platform failed os.chdir() will trigger WindowsError exception
    """
    print('Error: failed to change directory to: %s' % OUTPUT_PATH)

except OSError:
    """
    On Linux platform failed os.chdir() will trigger OSError exception
    """
    print('Error: failed to change directory to: %s' % OUTPUT_PATH)
except KeyboardInterrupt:
    print('\nCTRL+C pressed. Stopping dev server.')
    if curdir:
        os.chdir(curdir)
    print('Done.')