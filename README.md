[![build status](https://gitlab.com/alekc/pelican/badges/master/build.svg)](https://gitlab.com/alekc/pelican/commits/master)

----

Example [Pelican] website using GitLab Pages supporting Windows.

This is fork from original GitLab Pages repo: https://gitlab.com/pages/pelican

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation http://doc.gitlab.com/ee/pages/README.html.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
  - [Mixing Pelican with additional code](#mixing-pelican-with-additional-code)
- [Building locally](#building-locally)
- [Developing and Running your site on Windows](#developing-and-running-your-site-on-windows)
  - [Detecting Windows platform](#detecting-windows-platform)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [```.gitignore``` content](#gitignore-content)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:2.7-alpine

pages:
  script:
  - pip install -r requirements.txt
  - pelican -s publishconf.py
  artifacts:
    paths:
    - public/
```

For more details about using [.gitlab-ci.yml][glp] in Pages, please 
read official [README][glp].

### Mixing Pelican with additional code

In some cases it may be desired to mix Pelican-based site with 
additional code. In such case you may create additional branch and limit
GitLab CI to build only this particular branch. This will enable you
commit code to one branch, and your static site content to another.

Please remember about adding ```only``` keyword to ```.gitlab-ci.yml```
file. [Here][glpcode] is the step by step instruction how to do it.

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install] Pelican
1. Generate the website: `make html`
1. Preview your project: `make serve`
1. Add content

Read more at Pelican's [documentation].

## Developing and Running your site on Windows

Before running any of ```make-*.py``` scripts be sure to create
__public/__ directory in root of your project:

```
mkdir public
```

Pelican can be run on Windows successfully. Here are some addition to
the repository that makes the whole process easier:

1. `get-pelican-plugins.py` - downloads using git pelican plugins. From
now on your plugins are in __pelican-plugins__ directory. Point
```PLUGIN_PATHS``` variable in your ```pelicanconf.py``` file to this
directory. Do not forgot to enable your plugins by editing ```PLUGINS``` 
variable in ```pelicanconf.py``` 
1. `get-pelican-themes.py` - downloads using git pelican themes 
resulting in themes being put into __pelican-themes__ directory. Do not
use ```pelican-themes``` command as magaing pelican themes with this
tool is a nightmare. Instead just point your ```PELICAN_THEME``` 
variable from ```pelicanconf.py``` to __pelican-themes\your-theme__.
1. `make-html.py` is ```make html``` equivalent on Windows.
1. `make-autoreload-html.py` assures Pelican will autoreload content in
a infinite loop. Can be sometimes useful. 
1. `make-devserver` is ````make devserver``` equivalent on Windows.

Before committing and building your site be sure to edit ```.gitignore```
file accordingly in order to include your plugins and theme(s) while
excluding the rest of __pelican-plugins__ and __pelican-themes__
directories. 

Please note that thanks to Python you can use those scripts on other
platforms as well. 

If you are using ```virtualenv``` you ma consider switching to 
```virtualenvwrapper```. This makes working on a blog a lot easier:

```
mkvirtualenv pelican
pip install -r requirements.txt
```

Later to activate Pelican virtualenv just type: ```workon pelican```.

On Windows install [virtualenvwrapper-win][venvwin].

### Detecting Windows platform

```pelicanconf.py``` contains new boolean variable: ```isWindows``` 
set to ```True``` if running under Windows. This can be used to setup
directory paths correctly.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## ```.gitignore``` content

Supplied ```.gitignore``` file already cantains rules for ignoring
some of Pelican files including ```public/``` and ```output/```
directories plus ```*.pid``` files created by Pelican devserver. 

All ```*.pyc``` files are being ignored as well. 

If you place your virtualenv in  __venv/__ directory it will be
ignored too. However as noted earlier it makes more sens to deploy
Pelican with virtualenvwrapper. This will keep your root project
directory clean. 

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

1. No ```output``` directory. 

    While by default Pelican uses ```output``` directory this project
    sets it to ```public```. This is not only a leftover from original 
    repository but also a GitLab Pages requirement. 
    You may rerun ```pelican-quickstart``` to change it quickly or edit
    ```pelicanconf.py``` file manually, but than ```.gilab-ci.yml``` 
    will be __broken__ and you will not able to host your page on 
    GitLab. __You've been warned__. However if you do not plan to host 
    on GitLab, but only looking for Windows scripts you may change the output
    directory to anything else.

[ci]: https://about.gitlab.com/gitlab-ci/
[Pelican]: http://blog.getpelican.com/
[install]: http://docs.getpelican.com/en/3.6.3/install.html
[documentation]: http://docs.getpelican.com/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[glp]: http://docs.gitlab.com/ee/pages/README.html#explore-the-contents-of-gitlab-ciyml
[glpcode]: http://docs.gitlab.com/ee/pages/README.html#how-to-set-up-gitlab-pages-in-a-repository-where-theres-also-actual-code
[venvwin]: https://pypi.python.org/pypi/virtualenvwrapper-win